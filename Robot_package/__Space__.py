#coding: utf-8 -*-

#export PYTHONPATH=$pythonpath:"."

class Grid:
    """ 
    Ceci est une class qui répertorie différentes positions représentées par plusieurs unique Id.
    """
    def __init__(self):
        """ 
        Création du dictionnaire pour stocker les positions

        Output : dico
        """
        self.dico = {}

    def __str__(self):
        """
        Renvoie la façon dont on veut afficher le dictionnaire lors d'un print
        """
        return(str(self.dico))

    def addPosition(self, id1, x1, y1, r1):
        """
        Ajoute un élément au dictionnaire.

        Input : id1 (id) à ajouter)
                x1 (abscisse du point a ajouter)
                y1 (ordonnée du point a ajouter)
                r1 (rayon du point a ajouter)

        """
        if id1 in self.dico :
            print("Erreur : cet ID ("+id1+") existe déjà")
        else :
            if (r1>=0) :
                self.dico[id1] = [x1,y1,r1]
            else :
                print("Erreur : le rayon entré ("+str(r1)+") est négatif. Position non crée.")

    def removePosition(self, id1):
        """
        Supprime un élément du dictionnaire séléctionné par son Id
        """
        del self.dico[id1]

    def ListPosition(self):
        """
        Affiche les positions de touts les points enregistrés
        """
        print(self)

    def Position(self, id1):
        """
        Renvoie la position du point de l'id souhaité
        """
        return self.dico[id1]
    
    def Distance(self, id1, id2):
        """
        Renvoie la distance euclidienne entre les points d'id1 et d'id2
        """
        dx = (self.Position(id1)[0] - self.Position(id2)[0] )
        dy = (self.Position(id1)[1] - self.Position(id2)[1] )
        distance = (dx**2 + dy**2)**0.5
        return distance

    def Collision(self, id1, id2):
        """
        Renvoie True si les point d'id1 et 2 sont en collision, False sinon
        """
        collision = False
        distance = self.Distance(id1, id2)
        if ( distance < self.dico[id1][2] ) or (distance < self.dico[id2][2]):
            collision = True
        return collision
    

    

class SafeGrid:
    """ 
    Ceci est une class qui répertorie différentes positions représentées par plusieurs unique Id.
    """
    def __init__(self):
        """ 
        Création du dictionnaire pour stocker les positions

        Output : dico
        """
        self.dico = {}

    def __str__(self):
        """
        Renvoie la façon dont on veut afficher le dictionnaire lors d'un print
        """
        return(str(self.dico))

    def SafeaddPosition(self, id1, x1, y1, r1):
        """
        Ajoute un élément au dictionnaire si cela ne crée aucune collision.

        Input : id1 (id) à ajouter)
                x1 (abscisse du point a ajouter)
                y1 (ordonnée du point a ajouter)
                r1 (rayon du point a ajouter)

        """
        if id1 in self.dico :
            print("Erreur : cet ID ("+id1+") existe déjà")
        else :
            if (r1>=0) :
                safe = True
                self.dico[id1] = [x1, y1, r1]
                for element in self.dico:
                    if element != id1 :
                        if self.Collision(element, id1) :
                            safe = False
                if safe == False :
                    print("Erreur : la position ("+str(self.Position(id1))+") n'est pas safe. Position non crée")
                    self.removePosition(id1)
            else :
                print("Erreur : le rayon entré ("+str(r1)+") est négatif. Position non crée.")

    def SafenewPosition(self, id1, x1, y1):
        """
        Déplace si la position finale n'est pas en collision avec un autre objet.
        """
        safe = True
        oldPosition = self.Position(id1)
        r1=oldPosition[2]
        self.dico[id1] = [x1,y1,r1]
        for element in self.dico:
            if element != id1 :
                if self.Collision(element,id1) :
                    safe = False
        if safe == False :
            print("Erreur : la position ("+str(self.Position(id1))+") n'est pas safe. Position non déplacée")
            self.dico[id1] = oldPosition

    def removePosition(self, id1):
        """
        Supprime un élément du dictionnaire séléctionné par son Id
        """
        del self.dico[id1]

    def ListPosition(self):
        """
        Affiche les positions de touts les points enregistrés
        """
        print(self)

    def Position(self, id1):
        """
        Renvoie la position du point de l'id souhaité
        """
        return self.dico[id1]
    
    def Distance(self, id1, id2):
        """
        Renvoie la distance euclidienne entre les points d'id1 et d'id2
        """
        dx = (self.Position(id1)[0] - self.Position(id2)[0] )
        dy = (self.Position(id1)[1] - self.Position(id2)[1] )
        distance = (dx**2 + dy**2)**0.5
        return distance

    def Collision(self, id1, id2):
        """
        Renvoie True si les point d'id1 et 2 sont en collision, False sinon
        """
        collision = False
        distance = self.Distance(id1,id2)
        if ( distance < self.dico[id1][2] ) or (distance < self.dico[id2][2]):
            collision = True
        return collision




class SafeGrid:
    """ 
    Ceci est une class qui répertorie différentes positions représentées par plusieurs unique Id.
    """
    def __init__(self):
        """ 
        Création du dictionnaire pour stocker les positions

        Output : dico
        """
        self.dico = {}

    def __str__(self):
        """
        Renvoie la façon dont on veut afficher le dictionnaire lors d'un print
        """
        return(str(self.dico))

    def SafeaddPosition(self, id1, x1, y1, r1):
        """
        Ajoute un élément au dictionnaire si cela ne crée aucune collision.

        Input : id1 (id) à ajouter)
                x1 (abscisse du point a ajouter)
                y1 (ordonnée du point a ajouter)
                r1 (rayon du point a ajouter)

        """
        if id1 in self.dico :
            print("Erreur : cet ID ("+id1+") existe déjà")
        else :
            if (r1>=0) :
                safe = True
                self.dico[id1] = [x1, y1, r1]
                for element in self.dico:
                    if element != id1 :
                        if self.Collision(element, id1) :
                            safe = False
                if safe == False :
                    print("Erreur : la position ("+str(self.Position(id1))+") n'est pas safe. Position non crée")
                    self.removePosition(id1)
            else :
                print("Erreur : le rayon entré ("+str(r1)+") est négatif. Position non crée.")

    def SafenewPosition(self, id1, x1, y1):
        """
        Déplace si la position finale n'est pas en collision avec un autre objet.
        """
        safe = True
        oldPosition = self.Position(id1)
        r1=oldPosition[2]
        self.dico[id1] = [x1,y1,r1]
        for element in self.dico:
            if element != id1 :
                if self.Collision(element,id1) :
                    safe = False
        if safe == False :
            print("Erreur : la position ("+str(self.Position(id1))+") n'est pas safe. Position non déplacée")
            self.dico[id1] = oldPosition

    def removePosition(self, id1):
        """
        Supprime un élément du dictionnaire séléctionné par son Id
        """
        del self.dico[id1]

    def ListPosition(self):
        """
        Affiche les positions de touts les points enregistrés
        """
        print(self)

    def Position(self, id1):
        """
        Renvoie la position du point de l'id souhaité
        """
        return self.dico[id1]
    
    def Distance(self, id1, id2):
        """
        Renvoie la distance euclidienne entre les points d'id1 et d'id2
        """
        dx = (self.Position(id1)[0] - self.Position(id2)[0] )
        dy = (self.Position(id1)[1] - self.Position(id2)[1] )
        distance = (dx**2 + dy**2)**0.5
        return distance

    def Collision(self, id1, id2):
        """
        Renvoie True si les point d'id1 et 2 sont en collision, False sinon
        """
        collision = False
        distance = self.Distance(id1,id2)
        if ( distance < self.dico[id1][2] ) or (distance < self.dico[id2][2]):
            collision = True
        return collision
