#coding: utf-8 -*-
from Robot_package.__Space__ import Grid, SafeGrid
import unittest
import logging

class TestStringMethods(unittest.TestCase):
    """
    ceci permet de tester les différentes fonctions et de voir les éventuels problemes
    sans pour autant stopper
    """
    def setUp(self):
        """
        Initialisation
        """
        self.dico = Grid()
        self.dicosafe = SafeGrid()

    def test_addPosition(self):
        """
        Test de la fonction addPosition
        """
        logging.info("Debut test addPosition")
        self.dico.addPosition("0", 2, 2, 3)
        self.dico.addPosition("1", 2, 2, 3)

    def test_distance(self):
        """
        Test de la fonction distance
        """
        logging.info("Debut test distance")
        self.dico.addPosition("0", 2, 2, 3)
        self.dico.addPosition("1", 2, 2, 3)
        result = self.dico.Distance("0", "1")
        self.assertEqual(result, 0)

        self.dico.addPosition("2", 2, 2, 1)
        self.dico.addPosition("3", 5, 6, 0)
        result = self.dico.Distance("2", "3")
        self.assertEqual(result, 5)

    def test_collision(self):
        """
        Test de la fonction collision
        """
        logging.info("Debut test collision")
        self.dico.addPosition("0", 2, 2, 3)
        self.dico.addPosition("1", 2, 3, 3)
        result = self.dico.Collision("0", "1")
        self.assertEqual(result, True)

        self.dico.addPosition("2", 2, 2, 1)
        self.dico.addPosition("3", 5, 6, 0)
        result = self.dico.Collision("2", "3")
        self.assertEqual(result, False)

    def test_safeaddPosition(self):
        logging.info("Debut test safeaddPosition")
        self.dicosafe.SafeaddPosition("0", 2, 3, 3)
        self.dicosafe.SafeaddPosition("1", 3, 3, 1)

    def test_safenewPosition(self):
        logging.info("Debut test safenewPosition")
        self.dicosafe.SafeaddPosition("0", 2, 3, 3)
        self.dicosafe.SafenewPosition("0", 3, 3)

if __name__ == '__main__':
    logging.basicConfig(filename='test.log', level=logging.INFO)
    logging.info('On commence le test')
    unittest.main()
    logging.info('test terminé')
